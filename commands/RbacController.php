<?php
namespace app\commands;

use app\models\User;
use Yii;
use yii\console\Controller;
use yii\db\Exception;

class RbacController extends Controller
{
    public function actionInit()
    {
        $auth = Yii::$app->authManager;

        try {
            $adminRole = $auth->createRole('admin');
            $auth->add($adminRole);
            $auth->add($auth->createRole('head'));
            $auth->add($auth->createRole('manager'));
        } catch ( Exception $exception) { }

        try {
            $admin = new User();
            $admin->id = 0;
            $admin->username = 'abadmin';
            $admin->setPassword('abadmin2020');
            $admin->generatePasswordResetToken();
            $admin->generateAuthKey();
            $admin->email = 'abadmin@alfabank.kz';
            $admin->status = User::STATUS_ACTIVE;
            $admin->created_at = date('Y-m-d H:i:s');
            $admin->updated_at = date('Y-m-d H:i:s');
            $admin->save();
        } catch ( Exception $exception ) {}

        $admin = $auth->getRole('admin');
        $admin->description = 'Администратор';

        $head = $auth->getRole('head');
        $head->description = 'Начальник';

        $manager = $auth->getRole('manager');
        $manager->description = 'Менеджер';

        $auth->assign($auth->getRole('admin'), 0);

    }
}
