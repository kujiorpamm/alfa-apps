var increment_value = 1000;
var state = {
    is_processed: false,
    json: null,
    total: 0,
    increment: 0,
    increment_value: increment_value,
    filename:null,
    download:null
}

$('#start_export').on('click', function() {
   state.json = $('#polls_form').serialize();
   console.log(state.json);
    processPart();
});

function processPart() {

    $.ajax({
        url: exportLink,
        type: 'POST',
        dataType: 'JSON',
        data: {
            state: state
        },
        success: onProcessSuccess,
        error: onProcessError,
        beforeSend: function() {
            $('#btns_container').hide();
        }
    });
}


function onProcessSuccess(data) {

    state = data;

    if(state.is_processed) {
        state.increment += increment_value;
        var percentage = (state.increment / data.total * 100).toFixed(0);
        percentage = percentage > 100 ? 100 : percentage;
        $('#status_row').html( percentage + '%' );
        processPart();
    } else {
        $('#btns_container').show();
        Download(state.download);
        state = {
            is_processed: false,
            json: null,
            total: 0,
            increment: 0,
            increment_value: increment_value,
            filename:null,
            download:null
        };

    }

    console.log(data);
}
function onProcessError() {

}

function Download(url) {
    document.getElementById('downloadFrame').src = url;
};
