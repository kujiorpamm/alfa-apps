<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Applications */

$this->title = "Заявка #" . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Все заявки', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="applications-view">

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'date_created',
            [
                'attribute' => 'region_id',
                'value' => function ($model) {
                    return $model->other['region'];
                }
            ],
            [
                'attribute' => 'subject_id',
                'value' => function ($model) {
                    return $model->other['subject'];
                }
            ],
            [
                'attribute' => 'product_id',
                'value' => function ($model) {
                    return $model->other['product'];
                }
            ],
            [
                'attribute' => 'currency_id',
                'value' => function ($model) {
                    return $model->other['currency'];
                }
            ],
            [
                'attribute' => 'author_name',
                'label' => 'Автор',
                'value' => function ($model) {
                    return $model->other['author'];
                }
            ],
            'client_iin',
            'client_name',
            'value',
            'sum',
            'commend:ntext',
        ],
    ]) ?>

</div>
