<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Applications */

$this->title = 'Добавление новой заявки';
$this->params['breadcrumbs'][] = ['label' => 'Заявки', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="applications-create">

    <?= $this->render('_form', [
        'model' => $model,
        'regions' => $regions,
        'subjects' => $subjects,
        'products' => $products,
        'currency' => $currency,
        'managers' => $managers,
    ]) ?>

</div>
