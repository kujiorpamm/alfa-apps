<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Applications */

$this->title = 'Редактирование заявки №' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Заявки', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => "№" . $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="applications-update">

    <?= $this->render('_form', [
        'model' => $model,
        'regions' => $regions,
        'subjects' => $subjects,
        'products' => $products,
        'currency' => $currency,
        'managers' => $managers,
    ]) ?>

</div>
