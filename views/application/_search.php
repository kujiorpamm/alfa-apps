<?php

use kartik\daterange\DateRangePicker;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\search\ApplicationsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="applications-search">

    <?php $form = ActiveForm::begin([
        'action' => [Yii::$app->controller->action->id],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?=$form->field($model, 'date_created')->widget(DateRangePicker::classname(), [
        'useWithAddon'=>false,
        'model'=>$model,
        'attribute'=>'date_created',
        'options' => [
            'placeholder' => Yii::t('app', 'Дата'),
            'class' => 'form-control',
        ],
        'startAttribute' => 'date_start',
        'endAttribute' => 'date_end',
        //'convertFormat'=>true,
        'presetDropdown'=>true,
        'pluginOptions' => [
            'alwaysShowCalendars' => true,
            'opens'=>'right',
            'locale' => [
                'format' => 'DD.MM.YYYY',
            ]
        ]
    ])->label(false);?>

    <?= $form->field($model, 'region_id') ?>

    <?= $form->field($model, 'subject_id') ?>

    <?= $form->field($model, 'product_id') ?>

    <?php // echo $form->field($model, 'currency_id') ?>

    <?php // echo $form->field($model, 'author_id') ?>

    <?php // echo $form->field($model, 'client_iin') ?>

    <?php // echo $form->field($model, 'client_name') ?>

    <?php // echo $form->field($model, 'value') ?>

    <?php // echo $form->field($model, 'sum') ?>

    <?php // echo $form->field($model, 'commend') ?>

    <?php // echo $form->field($model, 'other') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
