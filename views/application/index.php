<?php

use app\assets\AppAsset;
use kartik\daterange\DateRangePicker;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\search\ApplicationsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Заявки';
$this->params['breadcrumbs'][] = $this->title;

$columns = [
    'id',
    [
        'attribute' => 'date_created',
        'filter' => Yii::$app->controller->action->id == 'all' ? DateRangePicker::widget([
            'useWithAddon'=>false,
            'model'=>$searchModel,
            'attribute'=>'date_created',
            'options' => [
                'placeholder' => Yii::t('app', 'Дата'),
                'class' => 'form-control',
            ],
            'startAttribute' => 'date_start',
            'endAttribute' => 'date_end',
            //'convertFormat'=>true,
            'presetDropdown'=>true,
            'pluginOptions' => [
                'alwaysShowCalendars' => true,
                'opens'=>'right',
                'locale' => [
                    'format' => 'YYYY-MM-DD',
                ]
            ]
        ]) : false,
    ],
    [
        'attribute' => 'region_id',
        'value' => function ($model) {
            return $model->other['region'];
        },
        'filter' => isset($regions) ? $regions : null
    ],
    [
        'attribute' => 'subject_id',
        'value' => function ($model) {
            return $model->other['subject'];
        },
        'filter' => isset($subjects) ? $subjects : null
    ],
    [
        'attribute' => 'product_id',
        'value' => function ($model) {
            return $model->other['product'];
        },
        'filter' => isset($products) ? $products : null
    ],
    [
        'attribute' => 'currency_id',
        'value' => function ($model) {
            return $model->other['currency'];
        },
        'filter' => isset($currency) ? $currency : null
    ],
    [
        'attribute' => 'author_name',
//                'label' => 'Менеджер',
        'value' => function ($model) {
            return $model->other['author'];
        }
    ],
    // TODO: Вынести это в отдельное поле
    [
        'attribute' => 'author_tabel_number',
        'label' => 'Табельный номер',
        'value' => function ($model) {
            return $model->author->tabel_number;
        }
    ],
    'client_iin',
    'client_name',
    [
        'attribute' => 'value',
        'filter' => false
    ],
    [
        'attribute' => 'sum',
        'filter' => false
    ],
    [
        'attribute' => 'commend',
        'filter' => false
    ],
    [
        'class' => 'yii\grid\ActionColumn',
        'template' => '{view}'
    ],
];

$extra_columns = [];
if(Yii::$app->user->can('admin')) {
    $extra_columns = [
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{update}'
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{delete}'
        ],
    ];
}

$columns = array_merge($columns, $extra_columns);

?>

<?php
    $exportLink = \yii\helpers\Url::to(['/application/export-answers']);
    $this->registerJs("var exportLink = '{$exportLink}'", \yii\web\View::POS_HEAD);
    $this->registerJsFile( Yii::getAlias('@web/js/export-questions.js') , ['depends' => AppAsset::className()]);
?>

<div class="applications-index">


    <?php \yii\bootstrap\Modal::begin([
        'id' => 'export_answers',
        'header' => 'Выгрузить заявки в Excel',
        'clientOptions' => [
            'show' => false
        ]
    ]) ?>

    <div class="export-modal">

        <div class="query-status">
            <p id="status_row"> Нажмите кнопку, чтобы начать экспорт заявок. Дождитесь завершения формирования документа. </p>
            <iframe id="downloadFrame" style="display:none;"></iframe>
        </div>

        <div id="btns_container" class="btns">
            <button style="min-width: 190px" class="btn btn-success" id="start_export">Начать</button>
        </div>

    </div>

    <?php \yii\bootstrap\Modal::end() ?>

    <?php $form = ActiveForm::begin(['id' => 'polls_form', 'action' => \yii\helpers\Url::to(['/polls/default/export-list'])]); ?>
        <?= $form->field($searchModel, 'id')->hiddenInput()->label(false) ?>
        <?= $form->field($searchModel, 'date_created')->hiddenInput()->label(false) ?>
        <?= $form->field($searchModel, 'region_id')->hiddenInput()->label(false) ?>
        <?= $form->field($searchModel, 'subject_id')->hiddenInput()->label(false) ?>
        <?= $form->field($searchModel, 'product_id')->hiddenInput()->label(false) ?>
        <?= $form->field($searchModel, 'currency_id')->hiddenInput()->label(false) ?>
        <?= $form->field($searchModel, 'currency_id')->hiddenInput()->label(false) ?>
        <?= $form->field($searchModel, 'author_name')->hiddenInput()->label(false) ?>
    <?php ActiveForm::end(); ?>
    <p>
        <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>
        <?php
            if (Yii::$app->user->can('admin') || Yii::$app->user->can('head')) {
                echo Html::button('Выгрузить заявки в Excel', ['class' => 'btn btn-success', 'onclick' => '$("#export_answers").modal("show")']);
            }
        ?>
    </p>

<!--    --><?php //echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => Yii::$app->controller->action->id === 'index' ? null : $searchModel,
        'columns' => $columns,
    ]); ?>

</div>
