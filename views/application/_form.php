<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Applications */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="applications-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-sm-6">

            <?= $form->field($model, 'date_created')->widget(\kartik\datetime\DateTimePicker::className(), [
                'model' => $model,
                'attribute' => 'date_created',
                'disabled' => true,
                'readonly' => true
            ]) ?>

            <?= $form->field($model, 'client_iin')->widget(\yii\widgets\MaskedInput::className(), [
                'model' => $model,
                'attribute' => 'client_iin',
                'mask' => '999999999999',
            ])->textInput() ?>

            <?= $form->field($model, 'client_name')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'product_id')->widget(\kartik\widgets\Select2::className(), [
                'data' => $products,
                'maintainOrder' => true,
                'options' => [
                    'placeholder' => 'Продукт'
                ]
            ]) ?>

            <?= $form->field($model, 'currency_id')->widget(\kartik\widgets\Select2::className(), [
                'data' => $currency,
                'maintainOrder' => true,
                'options' => [
                    'placeholder' => 'Валюта'
                ]
            ]) ?>

            <div class="row">
                <div class="col-xs-6">
                    <?= $form->field($model, 'value')->textInput(['type' => 'number']) ?>
                </div>
                <div class="col-xs-6">
                    <?= $form->field($model, 'sum')->textInput(['type' => 'number']) ?>
                </div>
            </div>
        </div>
        <div class="col-sm-6">

            <?= $form->field($model, 'author_id')->widget(\kartik\widgets\Select2::className(), [
                'data' => $managers,
                'maintainOrder' => true,
                'options' => [
                    'placeholder' => 'Регион',
                    'disabled' => !Yii::$app->user->can('admin')
                ]
            ]) ?>

            <?= $form->field($model, 'region_id')->widget(\kartik\widgets\Select2::className(), [
                'data' => $regions,
                'maintainOrder' => true,
                'options' => [
                    'placeholder' => 'Регион',
                    'disabled' => !Yii::$app->user->can('admin')
                ]
            ]) ?>

            <?= $form->field($model, 'subject_id')->widget(\kartik\widgets\Select2::className(), [
                'data' => $subjects,
                'maintainOrder' => true,
                'options' => [
                    'placeholder' => 'Подразделение',
                    'disabled' => !Yii::$app->user->can('admin') && !Yii::$app->user->can('head')
                ]
            ]) ?>

            <?= $form->field($model, 'commend')->textarea(['rows' => 6]) ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
