<?php
    use \yii\helpers\Url;
?>

<aside class="main-sidebar">

    <section class="sidebar">

        <?php if (Yii::$app->user->can('admin')): ?>
            <?= dmstr\widgets\Menu::widget(
                [
                    'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                    'items' => [
                        ['label' => 'Меню', 'options' => ['class' => 'header']],
                        ['label' => Yii::$app->user->identity->role, 'options' => ['class' => 'header']],
                        ['label' => 'Профиль', 'icon' => 'user', 'url' => Url::to(['/site/profile']) ],
                        ['label' => 'Заявки', 'icon' => 'user', 'url' => Url::to(['/application']) ],
                        ['label' => 'Все заявки', 'icon' => 'user', 'url' => Url::to(['/application/all']) ],
                        ['label' => 'Пользователи', 'icon' => 'user', 'url' => Url::to(['/admin/user']) ],
                        ['label' => 'Справочник', 'icon' => '', 'url' => Url::to(['/admin/catalog']) ],
                        ['label' => 'Вход', 'url' => ['/site/login'], 'visible' => Yii::$app->user->isGuest],
                    ],
                ]
            ) ?>
        <?php endif; ?>

        <?php if (Yii::$app->user->can('manager')): ?>
            <?= dmstr\widgets\Menu::widget(
                [
                    'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                    'items' => [
                        ['label' => 'Меню', 'options' => ['class' => 'header']],
                        ['label' => Yii::$app->user->identity->role, 'options' => ['class' => 'header']],
                        ['label' => 'Профиль', 'icon' => 'user', 'url' => Url::to(['/site/profile']) ],
                        ['label' => 'Заявки', 'url' => Url::to(['/application']) ],
                        ['label' => 'Все заявки', 'url' => Url::to(['/application/all']) ],
                        ['label' => 'Вход', 'url' => ['/site/login'], 'visible' => Yii::$app->user->isGuest],
                    ],
                ]
            ) ?>
        <?php endif; ?>

        <?php if (Yii::$app->user->can('head')): ?>
            <?= dmstr\widgets\Menu::widget(
                [
                    'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                    'items' => [
                        ['label' => 'Меню', 'options' => ['class' => 'header']],
                        ['label' => Yii::$app->user->identity->role, 'options' => ['class' => 'header']],
                        ['label' => 'Профиль', 'icon' => 'user', 'url' => Url::to(['/site/profile']) ],
                        ['label' => 'Заявки', 'url' => Url::to(['/application']) ],
                        ['label' => 'Все заявки', 'url' => Url::to(['/application/all']) ],
                        ['label' => 'Вход', 'url' => ['/site/login'], 'visible' => Yii::$app->user->isGuest],
                    ],
                ]
            ) ?>
        <?php endif; ?>

    </section>

</aside>
