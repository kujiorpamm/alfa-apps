<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\search\CatalogSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Справочник';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="catalog-index">

    <p>
        <?= Html::a('Добавить Регион', ['create-region'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Добавить Продукт', ['create-product'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Добавить Валюту', ['create-currency'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'name',
                'filter' => [
                    \app\models\Catalog::NAME_CITIES => 'Города',
                    \app\models\Catalog::NAME_CURRENCY => 'Валюта',
                    \app\models\Catalog::NAME_PRODUCTS => 'Продукты',
                ]
            ],
            'value',
            [
                'attribute' => 'parent_id',
                'value' => 'parent.value'
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {update}'
            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
