<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->title = 'Редактировать пользователя: ' . $model->username;
$this->params['breadcrumbs'][] = ['label' => 'Пользователи', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->username, 'url' => ['view', 'id' => $model->username]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="user-update">


    <div class="row">
        <div class="col-sm-6">
            <?php $form = ActiveForm::begin(['id' => 'chg', 'action' => \yii\helpers\Url::to(['/admin/user/update?id=' . $model->id])]); ?>
            <div class="panel panel-default" style="max-width: 768px;">
                <div class="panel-heading">Редактирование данных пользователя</div>
                <div class="panel-body">
                    <?= $form->field($model, 'subject_id')->widget(\kartik\widgets\Select2::className(), [
                        'data' => $region_subjects,
                        'maintainOrder' => true,
                        'options' => [
                            'placeholder' => 'Выберите регион'
                        ],
                        'pluginOptions' => [
                            'multiple' => false
                        ]
                    ])->label('Подразделение') ?>
                    <?= $form->field($model, 'name')->textInput(['autofocus' => true]) ?>
                    <?= $form->field($model, 'tabel_number')->textInput() ?>

                    <?php if (Yii::$app->user->can('admin')) : ?>

                    <?= $form->field($model, 'username')->textInput() ?>
                    <?= $form->field($model, 'email') ?>
                    <?= $form->field($model, 'role')->dropDownList($auth_items, ['prompt' => 'Выберите роль']) ?>
                    <?= $form->field($model, 'status')->dropDownList($statuses) ?>

                    <?php endif; ?>

                </div>
                <div class="panel-footer">
                    <div class="form-group">
                        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
                    </div>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
        <div class="col-sm-6">
            <?php $form = ActiveForm::begin(['id' => 'upd',  'action' => \yii\helpers\Url::to('/admin/user/update-password?id=' . $model->id)]); ?>
            <div class="panel panel-default" style="max-width: 768px;">
                <div class="panel-heading">Изменение пароля</div>
                <div class="panel-body">
                    <?= $form->field($model, 'password')->textInput(['autofocus' => true])->label('Новый пароль') ?>
                    <?= $form->field($model, 'password_repeat') ?>
                </div>
                <div class="panel-footer">
                    <div class="form-group">
                        <?= Html::submitButton('Изменить пароль', ['class' => 'btn btn-success']) ?>
                    </div>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>





</div>
