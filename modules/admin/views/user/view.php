<?php

use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->title = $model->username;
$this->params['breadcrumbs'][] = ['label' => 'Пользователи', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="user-view">

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <div class="panel panel-default">
        <div class="panel-body">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'id',
                    'username',
                    'email:email',
                    'statusName',
                    'created_at:datetime:Регистрация',
                    'updated_at:datetime:Обновление',
                ],
            ]) ?>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="clearfix">
                <div class="pull-left">Заметки</div>
                <div class="pull-right">
                    <?=Html::a("Добавить", \yii\helpers\Url::to(['/users/notes/create', 'id' => $model->id]), ['class' => 'btn btn-primary btn-xs']);?>
                </div>
            </div>
        </div>
        <div class="panel-body">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    'author_id',
                    'text:ntext',
                    'date_created',
                    'completed:boolean',

                    [
                        'header' => 'Выполнено?',
                        'class' => 'yii\grid\ActionColumn',
                        'template' => "{completed}",
                        'buttons' => [
                            'completed' => function($url, $model) {
                                $color = $model->completed ? "green" : 'grey';
                                return Html::a('<i class="fa fa-check" style="color:' . $color . ' "/>', \yii\helpers\Url::to(['/users/notes/mark', 'id' => $model->id]));
                            }
                        ]
                    ],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => "{view} {update} {delete}",
                        'urlCreator' => function ($action, $model, $key, $index) {
                            return \yii\helpers\Url::to(['/users/notes/' . $action , 'id' => $model->id]);
                        }
                    ],
                ],
            ]); ?>
        </div>
    </div>



</div>
