<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<?php $form = ActiveForm::begin(); ?>
<div class="panel panel-default" style="max-width: 768px;">

    <div class="panel-heading">Заполните форму пользователя</div>

   <div class="panel-body">

       <?= $form->field($model, 'name')->textInput() ?>

       <?= $form->field($model, 'tabel_number')->textInput() ?>

       <?= $form->field($model, 'region_id')->widget(\kartik\widgets\Select2::className(), [
           'data' => $region_subjects,
           'maintainOrder' => true,
           'options' => [
               'placeholder' => 'Выберите регион работы'
           ],
           'pluginOptions' => [
               'multiple' => false
           ]
       ]) ?>

       <?= $form->field($model, 'email') ?>

       <?= $form->field($model, 'password')->passwordInput() ?>

       <?= $form->field($model, 'password_repeat')->passwordInput() ?>

       <?= $form->field($model, 'role')->dropDownList($auth_items, ['prompt' => 'Выберите роль']) ?>

       <?= $form->field($model, 'status')->dropDownList($statuses) ?>

   </div>
    <div class="panel-footer">

        <div class="form-group">
            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
        </div>
    </div>

</div>

<?php ActiveForm::end(); ?>
