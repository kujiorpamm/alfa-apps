<?php

use yii\helpers\Html;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Модуль: Пользователи';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">
    <p>
        <?= Html::a('Добавить пользователя', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <div class="section">
        <div class="section-container">
            <?= \kartik\grid\GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    'id',
                    'username',
                    'name:text:Имя',
                    [
                        'label' => 'Роль',
                        'value' => 'role',
                        'value' => function ($model) {
                            return $model->roleByName;
                        }
                    ],
                    'email:email',
                    [
                        'attribute' => 'status',
                        'value' => 'statusName',
                        'filter' => \app\models\User::getStatuses()
                    ],
                    [
                        'class' => 'kartik\grid\DataColumn',
                        'attribute' => 'created_at',
                        'format' => 'datetime',
                        'filterType' => GridView::FILTER_DATE_RANGE,
                        'filterWidgetOptions' => [
                            'model' => $searchModel,
                            'attribute' => 'created_at',
                            'convertFormat'=>true,
                            'startAttribute' => 'created_start',
                            'endAttribute' => 'created_end',
                            'pluginOptions'=>[
                                'locale'=>['format' => 'Y-m-d'],
                            ]
                        ]
                    ],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => "{update}",
                    ],
                ],
            ]); ?>
        </div>
    </div>


</div>
