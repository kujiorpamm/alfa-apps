<?php

namespace app\modules\admin\controllers;

use app\models\Catalog;
use app\models\SignupForm;
use app\models\UpdateUserForm;
use app\models\AuthAssignment;
use app\models\AuthItem;
use app\models\Logger;
use Yii;
use app\models\User;
use app\models\search\UserSearch;
use yii\db\Expression;
use yii\filters\AccessControl;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * DefaultController implements the CRUD actions for User model.
 */
class UserController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['admin']
                    ]
                ],
            ],
        ];
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {

        $searchModel = new UserNotesSearch();
        $searchModel->user_id = $id;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('view', [
            'model' => $this->findModel($id),
            'dataProvider' => $dataProvider
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new SignupForm();
        $auth_items = AuthItem::find()->select('description')->indexBy('name')->column();
        $statuses = User::getStatuses();
        if ($model->load(Yii::$app->request->post()) && $model->signup()) {
            Yii::$app->session->setFlash('success', 'Пользователь добавлен!');
            return $this->redirect(['/admin/user']);
        }

        $region_subjects = Catalog::getRegionSubjects();

        return $this->render('create', [
            'model' => $model,
            'auth_items' => $auth_items,
            'statuses' => $statuses,
            'region_subjects' => $region_subjects,
        ]);
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->role = $model->getRoleByName();
        $update_model = new UpdateUserForm();
        $update_model->scenario = UpdateUserForm::SCENARIO_UPDATE_INFO;
        $update_model->id = $model->id;
        $update_model->name = $model->name;
        $update_model->tabel_number = $model->tabel_number;
        $update_model->username = $model->username;
        $update_model->email = $model->email;
        $update_model->role = $model->role;
        $update_model->status = $model->status;
        $update_model->region_id = $model->region_id;
        $update_model->subject_id = $model->subject_id;

        if ($update_model->load(Yii::$app->request->post()) && $update_model->validate()) {

            $model->username = $update_model->username;
            $model->name = $update_model->name;
            $model->tabel_number = $update_model->tabel_number;
            $model->email = $update_model->email;
            $model->status = $update_model->status;


            $subject = Catalog::findOne(['id' => $update_model->subject_id]);
            $model->region_id = $subject->parent_id;
            $model->subject_id = $update_model->subject_id;
            $model->setRoleByName($update_model->role);
            if($model->save()) {
                Yii::$app->session->setFlash('success', 'Данные сохранены');
                return $this->refresh();
            } else {
                Yii::$app->session->setFlash('error', 'Ошибка сохранения');
                Logger::add(['message' => 'Попытка обновить данные пользователя провалилась', 'errors' => $model->errors, 'model' => $model], 'User');
            }
        }

        $auth_items = AuthItem::find()->select('description')->indexBy('name')->column();
        $statuses = User::getStatuses();
        $region_subjects = Catalog::getRegionSubjects();

        return $this->render('update', [
            'model' => $update_model,
            'auth_items' => $auth_items,
            'statuses' => $statuses,
            'region_subjects' => $region_subjects,
        ]);
    }

    public function actionUpdatePassword($id) {
        $update_model = new UpdateUserForm();
        $update_model->scenario = UpdateUserForm::SCENARIO_CHANGE_PASSWORD;

        if ($update_model->load(Yii::$app->request->post()) && $update_model->validate()) {
            $model = $this->findModel($id);
            $model->setPassword($update_model->password);
            if($model->save()) {
                Yii::$app->session->setFlash('success', 'Пароль изменен ' . $update_model->password);
            } else {
                Yii::$app->session->setFlash('error', 'Не удалось изменить пароль');
            }
        } else {
            Yii::$app->session->setFlash('error', 'Не удалось изменить пароль');
        }
        return $this->redirect(['/admin/user/update?id=' . $id]);
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
