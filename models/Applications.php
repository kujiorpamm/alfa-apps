<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "applications".
 *
 * @property int $id
 * @property string|null $date_created
 * @property int $region_id
 * @property int $subject_id
 * @property int $product_id
 * @property int $currency_id
 * @property int $author_id
 * @property int $client_iin
 * @property string $client_name
 * @property int|null $value
 * @property int|null $sum
 * @property string|null $commend
 * @property string|null $other
 *
 * @property Catalog $region
 * @property Catalog $subject
 * @property Catalog $product
 * @property Catalog $currency
 * @property User $author
 */
class Applications extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'applications';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['date_created', 'other'], 'safe'],
            [['region_id', 'subject_id', 'product_id', 'currency_id', 'author_id', 'client_iin', 'client_name'], 'required'],
            [['region_id', 'subject_id', 'product_id', 'currency_id', 'author_id', 'client_iin', 'value', 'sum'], 'default', 'value' => null],
            [['region_id', 'subject_id', 'product_id', 'currency_id', 'author_id', 'client_iin', 'value', 'sum'], 'integer'],
            [['commend'], 'string'],
            [['client_name'], 'string', 'max' => 256],
            [['client_iin'], 'match', 'pattern' => '/^[0-9]{12}/'],
            [['region_id'], 'exist', 'skipOnError' => true, 'targetClass' => Catalog::className(), 'targetAttribute' => ['region_id' => 'id']],
            [['subject_id'], 'exist', 'skipOnError' => true, 'targetClass' => Catalog::className(), 'targetAttribute' => ['subject_id' => 'id']],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Catalog::className(), 'targetAttribute' => ['product_id' => 'id']],
            [['currency_id'], 'exist', 'skipOnError' => true, 'targetClass' => Catalog::className(), 'targetAttribute' => ['currency_id' => 'id']],
            [['author_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['author_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date_created' => 'Дата',
            'region_id' => 'Регион',
            'subject_id' => 'Подразделение',
            'product_id' => 'Продукт',
            'currency_id' => 'Валюта',
            'author_id' => 'Менеджер',
            'author_name' => 'Менеджер',
            'client_iin' => 'ИИН',
            'client_name' => 'Ф.И.О.',
            'value' => 'Количество',
            'sum' => 'Сумма',
            'commend' => 'Коментарий',
            'other' => 'Other',
        ];
    }

    /**
     * Gets query for [[Region]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRegion()
    {
        return $this->hasOne(Catalog::className(), ['id' => 'region_id']);
    }

    /**
     * Gets query for [[Subject]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSubject()
    {
        return $this->hasOne(Catalog::className(), ['id' => 'subject_id']);
    }

    /**
     * Gets query for [[Product]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Catalog::className(), ['id' => 'product_id']);
    }

    /**
     * Gets query for [[Currency]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCurrency()
    {
        return $this->hasOne(Catalog::className(), ['id' => 'currency_id']);
    }

    /**
     * Gets query for [[Author]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(User::className(), ['id' => 'author_id']);
    }

    public function beforeSave($insert)
    {
        $this->other = [
            'product' => $this->product->value,
            'currency' => $this->currency->value,
            'author' => $this->author->name,
            'authorLogin' => $this->author->username,
            'authorTabel' => $this->author->tabel_number,
            'region' => $this->region->value,
            'subject' => $this->subject->value,
        ];
        return parent::beforeSave($insert);
    }

}
