<?php
namespace app\models;

use Yii;
use yii\base\Model;
use app\models\User;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $username;
    public $email;
    public $password;
    public $password_repeat;
    public $role;
    public $status;
    public $name;
    public $tabel_number;
    public $region_id;
    public $phone;


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['username', 'trim'],
            [['name'], 'required'],
            ['username', 'unique', 'targetClass' => '\app\models\User', 'message' => 'Это имя для входа уже занято'],
            ['username', 'string', 'min' => 2, 'max' => 255],
            ['tabel_number', 'string', 'min' => 2, 'max' => 255],

            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\app\models\User', 'message' => 'Этот email уже занят'],

            ['region_id', 'integer'],

            ['password_repeat', 'compare', 'compareAttribute' => 'password', 'message' => 'Пароли не совпадают'],

            [ ['password', 'password_repeat', 'role', 'status'], 'required'],
            ['password', 'string', 'min' => 6],
        ];
    }

    public function attributeLabels()
    {
        return [
          'password' => 'Пароль',
          'password_repeat' => 'Подтвердите пароль',
          'username' => 'Имя для входа',
          'role' => 'Роль пользователя',
          'status' => 'Статус пользователя',
          'name' => 'Имя',
          'tabel_number' => 'Табельный номер',
          'region_id' => 'Регион',
        ];

    }

    /**
     * Signs user up.
     *
     * @return bool whether the creating new account was successful and email was sent
     */
    public function signup()
    {
        if (!$this->validate()) {

//            echo "<pre>"; var_dump($this->getErrors()); exit;

            return null;
        }

        $region = Catalog::findOne([
            'id' => $this->region_id
        ]);

        $user = new User();
        $user->username = $this->email;
        $user->email = $this->email;
        $user->status = $this->status;
        $user->name = $this->name;
        $user->tabel_number = $this->tabel_number;
        $user->region_id = $region->parent_id;
        $user->subject_id = $region->id;
        $user->setPassword($this->password);
        $user->generateAuthKey();


        $transaction = Yii::$app->db->beginTransaction();
        try {
            $user->save();
            $user->setRoleByName($this->role);
//            $this->sendEmail($user);
        } catch (\Exception $e) {
            $transaction->rollBack();
            var_dump($e->getMessage()); exit;
            return false;
        }
        $transaction->commit();
        return true;
    }

    /**
     * Sends confirmation email to user
     * @param User $user user model to with email should be send
     * @return bool whether the email was sent
     */
    protected function sendEmail($user)
    {
        return Yii::$app
            ->mailer
            ->compose(
                ['html' => 'emailVerify-html', 'text' => 'emailVerify-text'],
                ['user' => $user]
            )
            ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name . ' robot'])
            ->setTo($this->email)
            ->setSubject('Account registration at ' . Yii::$app->name)
            ->send();
    }
}
