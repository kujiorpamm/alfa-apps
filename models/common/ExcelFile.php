<?php

namespace app\models\common;

use Box\Spout\Writer\WriterFactory;
use Yii;
use yii\base\Model;

/* Потом сделать можно  */

class ExcelFile extends Model {

    static function WriteToFile($items, $filename, $isNew = false, $headers = null) {

        if ( $isNew ) {

        }
        else {

        }

        $writer = WriterFactory::create(Type::XLSX);
        $writer->openToFile($filename);

        /* Заголовки */
        if ($headers) $writer->addRow($headers);

        $tempname = Yii::getAlias("@webroot") . "/reports/" . Yii::$app->security->generateRandomString(8) . ".xlsx";

        /* Контент */
        foreach ($items as $poll) {
            $writer->addRow($poll);
        }

        $writer->close();

    }

}
