<?php
namespace app\models;

use Yii;
use yii\base\Model;
use app\models\User;

/**
 * Signup form
 */
class UpdateUserForm extends Model
{

    const SCENARIO_CHANGE_PASSWORD = '9';
    const SCENARIO_UPDATE_INFO = '10';

    public $id;
    public $username;
    public $email;
    public $password;
    public $password_repeat;
    public $role;
    public $status;
    public $name;
    public $tabel_number;
    public $region_id;
    public $subject_id;


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['username', 'trim'],
            ['username', 'required', 'on' => self::SCENARIO_UPDATE_INFO],
            ['id', 'integer'],
            ['username', 'validateNewUsername'],
            ['username', 'string', 'min' => 2, 'max' => 255],

            ['name', 'string'],

            ['email', 'trim'],
            ['email', 'required', 'on' => self::SCENARIO_UPDATE_INFO],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'validateNewEmail'],

            ['tabel_number', 'string', 'max' => 255],
            ['region_id', 'integer'],
            ['subject_id', 'integer'],

            ['password_repeat', 'compare', 'compareAttribute' => 'password', 'message' => 'Пароли не совпадают'],

            [ ['role', 'status'], 'required', 'on' => self::SCENARIO_UPDATE_INFO],
            [ ['password', 'password_repeat'], 'required', 'on' => self::SCENARIO_CHANGE_PASSWORD],
            ['password', 'string', 'min' => 6],
        ];
    }

    public function validateNewUsername($attribute, $params) {
        $dups = User::find()->where(['username' => $this->username])->andWhere(['!=', 'id', $this->id])->one();
        if($dups) {
            $this->addError($attribute, 'В системе уже есть друргой пользователь с этим Именем для входа');
        }
    }
    public function validateNewEmail($attribute, $params) {
        $dups = User::find()->where(['email' => $this->email])->andWhere(['!=', 'id', $this->id])->one();
        if($dups) {
            $this->addError($attribute, 'В системе уже есть друргой пользователь с этим Email');
        }
    }

    public function attributeLabels()
    {
        return [
          'password' => 'Пароль',
          'password_repeat' => 'Подтвердите пароль',
          'username' => 'Имя для входа',
          'role' => 'Роль пользователя',
          'status' => 'Статус пользователя',
          'name' => 'Имя',
            'tabel_number' => 'Табельный номер',
            'region_id' => 'Подразделение',
        ];

    }

    /**
     * Signs user up.
     *
     * @return bool whether the creating new account was successful and email was sent
     */
    public function update()
    {
        if (!$this->validate()) {
            return null;
        }

        $region = Catalog::findOne([
            'id' => $this->subject_id
        ]);

        $user = new User();
        $user->username = $this->username;
        $user->email = $this->email;
        $user->status = $this->status;
        $user->tabel_number = $this->tabel_number;
        $user->region_id = $region->parent_id;
        $user->subject_id = $region->id;
        $user->setPassword($this->password);
        $user->generateAuthKey();
        $user->generateEmailVerificationToken();
        try {
            $user->save();
            $user->setRoleByName($this->username);
            $this->sendEmail($user);
        } catch (\Exception $e) {
            return false;
        }
        return true;
    }

    protected function sendEmail($user)
    {
        return Yii::$app
            ->mailer
            ->compose(
                ['html' => 'emailVerify-html', 'text' => 'emailVerify-text'],
                ['user' => $user]
            )
            ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name . ' robot'])
            ->setTo($this->email)
            ->setSubject('Account registration at ' . Yii::$app->name)
            ->send();
    }
}
