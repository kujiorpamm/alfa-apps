<?php

namespace app\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Applications;
use yii\db\Expression;

/**
 * ApplicationsSearch represents the model behind the search form of `app\models\Applications`.
 */
class ApplicationsSearch extends Applications
{

    public $author_name;
    public $date_start;
    public $date_end;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'region_id', 'subject_id', 'product_id', 'currency_id', 'author_id', 'client_iin', 'value', 'sum'], 'integer'],
            [['date_created', 'client_name', 'commend', 'other', 'author_name', 'date_start', 'date_end'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Applications::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'author_id' => $this->author_id,
            'region_id' => $this->region_id,
            'subject_id' => $this->subject_id,
            'product_id' => $this->product_id,
            'currency_id' => $this->currency_id,
            'client_iin' => $this->client_iin,
            'value' => $this->value,
            'sum' => $this->sum,
        ]);

        if(preg_match('/\d{2}\.\d{2}\.\d{4} - \d{2}\.\d{2}\.\d{4}/', $this->date_created)) {
            $this->date_start = substr($this->date_created, 0, 10);
            $this->date_start = (new \DateTime($this->date_start))->format('Y-m-d');
            $this->date_end = substr($this->date_created, 13, 10);
            $this->date_end = (new \DateTime($this->date_end))->format('Y-m-d');
        }

//        var_dump($this->date_created, $this->date_start, $this->date_end); exit;


        if($this->date_created && !$this->date_start && !$this->date_end) {
            $query->andFilterWhere(['=', 'DATE(date_created)', $this->date_created]);
        }

        if($this->date_start && $this->date_end) {
            $query->andFilterWhere(['BETWEEN', 'DATE(date_created)', $this->date_start, $this->date_end]);
        }

        $query->andFilterWhere(['ilike', 'client_name', $this->client_name])
            ->andFilterWhere(['ilike', 'commend', $this->commend])
            ->andFilterWhere(['ilike', 'other', $this->other]);

        if($this->author_name) {
            $query->andWhere(new Expression("other ->> 'author' ilike '%$this->author_name%'"));
        }

        if($this->date_start && $this->date_end) {
            $this->date_created = "{$this->date_start} - {$this->date_end}";
        }
        return $dataProvider;
    }
}
