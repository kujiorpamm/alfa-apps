<?php

namespace app\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\User;

/**
 * UserSearch represents the model behind the search form of `common\models\User`.
 */
class UserSearch extends User
{
    /**
     * {@inheritdoc}
     */

    public $created_start;
    public $created_end;

    public function rules()
    {
        return [
            [['id', 'status', 'updated_at'], 'integer'],
            [['username', 'created_at', 'auth_key', 'password_hash', 'password_reset_token', 'email', 'verification_token', 'created_start', 'created_end'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = User::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'status' => $this->status,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['ilike', 'username', $this->username])
            ->andFilterWhere(['ilike', 'auth_key', $this->auth_key])
            ->andFilterWhere(['ilike', 'password_hash', $this->password_hash])
            ->andFilterWhere(['ilike', 'password_reset_token', $this->password_reset_token])
            ->andFilterWhere(['ilike', 'email', $this->email]);

        if($this->created_start && $this->created_end) {
            $this->created_start = \Yii::$app->formatter->asTimestamp($this->created_start . "00:00:00");
            $this->created_end = \Yii::$app->formatter->asTimestamp($this->created_end . "23:59:59");
            $query->andFilterWhere(['BETWEEN', 'created_at', $this->created_start, $this->created_end]);
        }

        return $dataProvider;
    }
}
