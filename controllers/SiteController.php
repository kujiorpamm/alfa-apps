<?php

namespace app\controllers;

use app\models\AuthItem;
use app\models\Catalog;
use app\models\UpdateUserForm;
use app\models\User;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login'],
                        'allow' => true
                    ],
                    [
                        'actions' => ['index', 'profile', 'logout'],
                        'allow' => true,
                        'roles' => ['admin', 'head', 'manager']
                    ]
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionProfile() {

        $model = User::findOne(['id' => Yii::$app->user->id]);
        $model->role = $model->getRoleByName();
        $update_model = new UpdateUserForm();
        $update_model->scenario = UpdateUserForm::SCENARIO_UPDATE_INFO;
        $update_model->id = $model->id;
        $update_model->name = $model->name;
        $update_model->tabel_number = $model->tabel_number;
        $update_model->username = $model->username;
        $update_model->email = $model->email;
        $update_model->role = $model->role;
        $update_model->status = $model->status;
        $update_model->region_id = $model->region_id;
        $update_model->subject_id = $model->subject_id;

        if ($update_model->load(Yii::$app->request->post()) && $update_model->validate()) {

            $model->username = $update_model->username;
            $model->name = $update_model->name;
            $model->tabel_number = $update_model->tabel_number;
            $model->email = $update_model->email;
            $model->status = $update_model->status;
            $model->region_id = $update_model->region_id;
            $model->subject_id = $update_model->subject_id;
            $model->setRoleByName($update_model->role);
            if($model->save()) {
                Yii::$app->session->setFlash('success', 'Данные сохранены');
                return $this->refresh();
            } else {
                Yii::$app->session->setFlash('error', 'Ошибка сохранения');
                Logger::add(['message' => 'Попытка обновить данные пользователя провалилась', 'errors' => $model->errors, 'model' => $model], 'User');
            }
        }

        $auth_items = AuthItem::find()->select('description')->indexBy('name')->column();
        $statuses = User::getStatuses();
        $region_subjects = Catalog::getRegionSubjects();

        return $this->render('@app/modules/admin/views/user/update', [
            'model' => $update_model,
            'auth_items' => $auth_items,
            'statuses' => $statuses,
            'region_subjects' => $region_subjects,
        ]);
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
}
