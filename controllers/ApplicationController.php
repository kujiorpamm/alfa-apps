<?php

namespace app\controllers;

use app\models\Catalog;
use app\models\User;
use Yii;
use app\models\Applications;
use app\models\search\ApplicationsSearch;
use yii\db\Exception;
use yii\db\Expression;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Writer\WriterFactory;
use Box\Spout\Common\Type;

/**
 * ApplicationController implements the CRUD actions for Applications model.
 */
class ApplicationController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['admin', 'head', 'manager']
                    ]
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Applications models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ApplicationsSearch();

        /* По умолчанию - за сегодня */
        if(!Yii::$app->request->queryParams) {
            $searchModel->date_created = date('Y-m-d');
        }
        /* Если начальник */
        if(Yii::$app->user->can('head')) {
            $searchModel->region_id = Yii::$app->user->identity->region_id;
        }
        /* Если менеджер */
        if(Yii::$app->user->can('manager')) {
            $searchModel->author_id = Yii::$app->user->id;
        }


        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionAll()
    {
        $searchModel = new ApplicationsSearch();
        $subjects = [];

        /* Эта хрень нужна, потому что глючит фильтр заявок */
        if (!Yii::$app->request->queryParams) {
            $searchModel->date_start = Yii::$app->db->createCommand("SELECT MIN(date_created) FROM applications")->queryScalar();
            $searchModel->date_end = Yii::$app->db->createCommand("SELECT MAX(date_created) FROM applications")->queryScalar();
        }

        /* Если начальник - все заявки региона */
        if(Yii::$app->user->can('head')) {
            $searchModel->region_id = Yii::$app->user->identity->region_id;
        }
        /* Если менеджер - только его заявки */
        if(Yii::$app->user->can('manager')) {
            $searchModel->author_id = Yii::$app->user->id;
        }

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'regions' => Yii::$app->user->identity->getAllowedRegionsList(),
            'subjects' => Yii::$app->user->identity->getAllowedSubjectList(),
            'products' => Catalog::find()->where(['=', 'name', Catalog::NAME_PRODUCTS])->select('value')->indexBy('id')->column(),
            'currency' => Catalog::find()->where(['=', 'name', Catalog::NAME_CURRENCY])->select('value')->indexBy('id')->column(),
        ]);
    }

    /* т.к. табельный номер добавлен чуть позже, его нет в JSON массиве
        его нужно добавить этоим экшеном. В будущем пригодится для обновления
        табельных номеров
     */
    public function actionFixTableNumber() {
        while($models = Applications::find()->where(['IS', new Expression("other ->> 'authorTabel'"), NULL])->limit(200)->all()) {
            foreach ($models as $model) {
                $other = $model->other;
                $other['authorTabel'] = $model->author->tabel_number;
                $model->other = $other;
                $model->save();
            }
            $models = NULL;
        }
        return $this->redirect(['/application']);
    }

    public function actionExportAnswers() {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $state = Yii::$app->request->post('state');
        parse_str($state['json'], $model_data) ;

        $increment = $state['increment'];
        $increment_by = $state['increment_value'];

        $searchModel = new ApplicationsSearch($model_data['ApplicationsSearch']);
        if(
            preg_match('/\d{2}\.\d{2}\.\d{4} - \d{2}\.\d{2}\.\d{4}/', $searchModel->date_created)
            || preg_match('/\d{4}-\d{2}-\d{2} - \d{4}-\d{2}-\d{2}/', $searchModel->date_created)
        ) {
            $searchModel->date_start = substr($searchModel->date_created, 0, 10);
            $searchModel->date_start = (new \DateTime($searchModel->date_start))->format('Y-m-d');
            $searchModel->date_end = substr($searchModel->date_created, 13, 10);
            $searchModel->date_end = (new \DateTime($searchModel->date_end))->format('Y-m-d');
        }
        if(
            preg_match('/\d{2}\.\d{2}\.\d{4} \d{2}:\d{2}:\d{2} - \d{2}\.\d{2}\.\d{4} \d{2}:\d{2}:\d{2}/', $searchModel->date_created)
            || preg_match('/\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2} - \d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}/', $searchModel->date_created)
        ) {
            $searchModel->date_start = substr($searchModel->date_created, 0, 10);
            $searchModel->date_start = (new \DateTime($searchModel->date_start))->format('Y-m-d');
            $searchModel->date_end = substr($searchModel->date_created, 23, 10);
            $searchModel->date_end = (new \DateTime($searchModel->date_end))->format('Y-m-d');
        }

        $dataProvider = $searchModel->search($model_data);


        if( !$state['filename'] ) {
            $file_hash = Yii::$app->security->generateRandomString(8) . ".xlsx";
            $download = \yii\helpers\Url::to(["/reports/" . $file_hash]);
            $filename = Yii::getAlias("@webroot") . '/reports/' . $file_hash;
        } else {
            $download = $state['download'];
            $filename = $state['filename'];
        }

        $total = $dataProvider->getTotalCount();
        $items = $dataProvider->query
            ->select( new Expression("id, other ->> 'author' as Автор, other ->> 'authorTabel' as ТабельныйНомер , other ->> 'authorLogin' as Логин, CONCAT(other->>'region', '/', other->>'subject') as Регион, other ->> 'product' as Продукт, date_created, client_name, client_iin, value, sum, other ->>'currency' as Валюта, commend") )
            ->orderBy('id')->offset($increment)->limit($increment_by);

        $sql = $dataProvider->query->createCommand()->rawSql;
        $items = Yii::$app->db->createCommand($sql)->queryAll();

//        var_dump($items); exit;

        $headers = [
            'ID в базе',
            'ФИО менеджера', 'таб.номер менеджера', 'Логин', 'Регион/Подразделение', 'Продукт',
            'Дата привлечения', 'ФИО клиента', 'ИИН клиента', 'Количество привлеченного продукта',
            'Сумма привлеченного продукта', 'Валюта привлеченного продукта', 'Комментарий'
        ];

        try {
            /* Если файл существует */
            if (file_exists($filename)) {
                $reader = ReaderFactory::create(Type::XLSX);
                $reader->open($filename);

                $tempname = Yii::getAlias("@webroot") . "/reports/" . Yii::$app->security->generateRandomString(8) . ".xlsx";

                $writer = WriterFactory::create(Type::XLSX);
                $writer->openToFile($tempname);

                foreach ($reader->getSheetIterator() as $sheetIndex => $sheet) {
                    foreach ($sheet->getRowIterator() as $row) {
                        $writer->addRow($row);
                    }
                }

                foreach ($items as $poll) {
                    $writer->addRow($poll);
                }

                $reader->close();
                $writer->close();

                unlink($filename);
                rename($tempname, $filename);
            }
            /* Если файл не существует */
            else {
                $row_headers = $headers;
                $writer = WriterFactory::create(Type::XLSX);
                $writer->openToFile($filename);
                /* Записать табельный номер, если существует */
                $writer->addRow(['Табельный номер: ', Yii::$app->user->identity->tabel_number]);
                $writer->addRow($row_headers);
                foreach ($items as $poll) {
                    $writer->addRow($poll);
                }
                $writer->close();
            }
        } catch (\Exception $e) {
            var_dump($e->getMessage(), $filename);
        }

        $state['increment'] = $increment + $increment_by;
        $state['filename'] = $filename;
        $state['download'] = $download;
        $state['is_processed'] = sizeof($items) > 0;
        $state['total'] = $total;

        return $state;
    }

    /**
     * Displays a single Applications model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Applications model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Applications();
        $model->date_created = date('Y-m-d H:i:s');

//        echo "<pre>"; var_dump(Catalog::getRegionSubjects()[Yii::$app->user->identity->region_id]); exit;

        $managers = User::find()->where(['status' => User::STATUS_ACTIVE])->select('name')->indexBy('id')->column();

        $model->region_id = Yii::$app->user->identity->region_id;
        $model->subject_id = Yii::$app->user->identity->subject_id;

        $model->author_id = Yii::$app->user->identity->id;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }

        return $this->render('create', [
            'model' => $model,
            'regions' => Yii::$app->user->identity->getAllowedRegionsList(),
            'subjects' => Yii::$app->user->identity->getAllowedSubjectList(),
            'products' => Catalog::getProducts(),
            'currency' => Catalog::getCurrency(),
            'managers' => $managers,
        ]);
    }

    /**
     * Updates an existing Applications model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $managers = User::find()->where(['status' => User::STATUS_ACTIVE])->select('name')->indexBy('id')->column();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $model,
            'regions' => Yii::$app->user->identity->getAllowedRegionsList(),
            'subjects' => Yii::$app->user->identity->getAllowedSubjectList(),
            'products' => Catalog::getProducts(),
            'currency' => Catalog::getCurrency(),
            'managers' => $managers,
        ]);
    }

    /**
     * Deletes an existing Applications model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * Finds the Applications model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Applications the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Applications::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
