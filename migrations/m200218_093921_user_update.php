<?php

use yii\db\Migration;

/**
 * Class m200218_093921_user_update
 */
class m200218_093921_user_update extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(\app\models\User::tableName(), 'name', $this->string(256)->notNull()->defaultValue('Неизвестно'));
        $this->addColumn(\app\models\User::tableName(), 'tabel_number', $this->string(256)->notNull()->defaultValue('Неизвестно'));

        $this->addColumn(\app\models\User::tableName(), 'region_id', $this->integer());
        $this->addColumn(\app\models\User::tableName(), 'subject_id', $this->integer());

        $this->createIndex('idx-usr-region', \app\models\User::tableName(), 'region_id');
        $this->addForeignKey('fk-usr-region', \app\models\User::tableName(), 'region_id', \app\models\Catalog::tableName(), 'id', 'CASCADE');

        $this->createIndex('idx-usr-subject', \app\models\User::tableName(), 'subject_id');
        $this->addForeignKey('fk-usr-subject', \app\models\User::tableName(), 'subject_id', \app\models\Catalog::tableName(), 'id', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200218_093921_user_update cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200218_093921_user_update cannot be reverted.\n";

        return false;
    }
    */
}
