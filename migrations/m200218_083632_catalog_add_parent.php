<?php

use yii\db\Migration;

/**
 * Class m200218_083632_catalog_add_parent
 */
class m200218_083632_catalog_add_parent extends Migration
{

    const tableName = 'catalog';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('catalog', 'parent_id', $this->integer());

        $idx = $this->createIndex('idx-cat-parent_id', self::tableName, 'parent_id');
        $this->addForeignKey('fk-cat-parent_id', self::tableName, 'parent_id', self::tableName, 'id', 'CASCADE');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200218_083632_catalog_add_parent cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200218_083632_catalog_add_parent cannot be reverted.\n";

        return false;
    }
    */
}
