<?php

use app\models\Catalog;
use yii\db\Migration;

/**
 * Class m200224_084109_applications_update
 */
class m200224_084109_applications_update extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropForeignKey('fk_currency', 'applications');
        $fk_currency = $this->addForeignKey('fk_currency', 'applications', 'currency_id', Catalog::tableName(), 'id', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200224_084109_applications_update cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200224_084109_applications_update cannot be reverted.\n";

        return false;
    }
    */
}
