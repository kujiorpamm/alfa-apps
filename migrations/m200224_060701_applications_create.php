<?php

use app\models\Catalog;
use app\models\User;
use yii\db\Migration;

/**
 * Class m200224_060701_applications_create
 */
class m200224_060701_applications_create extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('applications', [
            'id' => $this->primaryKey(),
            'date_created' => $this->timestamp(),
            'region_id' => $this->integer()->notNull(),
            'subject_id' => $this->integer()->notNull(),
            'product_id'  => $this->integer()->notNull(),
            'currency_id'  => $this->integer()->notNull(),
            'author_id'  => $this->integer()->notNull(),
            'client_iin' => $this->integer()->notNull(),
            'client_name' => $this->string(256)->notNull(),
            'value' => $this->integer(),
            'sum' => $this->integer(),
            'commend' => $this->text(2048),
            'other' => $this->json()
        ]);

        $idx_region = $this->createIndex('idx_region', 'applications', 'region_id');
        $fk_region = $this->addForeignKey('fk_region', 'applications', 'region_id', Catalog::tableName(), 'id', 'CASCADE');
        $idx_subject = $this->createIndex('idx_subject', 'applications', 'subject_id');
        $fk_subject = $this->addForeignKey('fk_subject', 'applications', 'subject_id', Catalog::tableName(), 'id', 'CASCADE');
        $idx_product = $this->createIndex('idx_product', 'applications', 'product_id');
        $fk_product = $this->addForeignKey('fk_product', 'applications', 'product_id', Catalog::tableName(), 'id', 'CASCADE');
        $idx_currency = $this->createIndex('idx_currency', 'applications', 'currency_id');
        $fk_currency = $this->addForeignKey('fk_currency', 'applications', 'currency_id', User::tableName(), 'id', 'CASCADE');
        $idx_author = $this->createIndex('idx_author', 'applications', 'author_id');
        $fk_author = $this->addForeignKey('fk_author', 'applications', 'author_id', User::tableName(), 'id', 'CASCADE');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('applications');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200224_060701_applications_create cannot be reverted.\n";

        return false;
    }
    */
}
