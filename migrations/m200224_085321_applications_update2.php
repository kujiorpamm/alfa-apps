<?php

use yii\db\Migration;

/**
 * Class m200224_085321_applications_update2
 */
class m200224_085321_applications_update2 extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('applications', 'client_iin');
        $this->addColumn('applications', 'client_iin', $this->string(12));

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200224_085321_applications_update2 cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200224_085321_applications_update2 cannot be reverted.\n";

        return false;
    }
    */
}
