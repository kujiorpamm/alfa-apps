<?php

use yii\db\Migration;

/**
 * Class m200218_081851_catalog_create
 */
class m200218_081851_catalog_create extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('catalog', [
            'id' => $this->primaryKey(),
            'name' => $this->string(256)->notNull(),
            'value' => $this->string(512)->notNull()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('catalog');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200218_081851_catalog_create cannot be reverted.\n";

        return false;
    }
    */
}
