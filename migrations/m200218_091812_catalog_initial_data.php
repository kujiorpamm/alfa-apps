<?php

use yii\db\Migration;

/**
 * Class m200218_091812_catalog_initial_data
 */
class m200218_091812_catalog_initial_data extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $model = new \app\models\Catalog();
        $model->parent_id = NULL;
        $model->name = \app\models\Catalog::NAME_CITIES;
        $model->value = 'Главная категория (Регионы)';
        $model->save();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200218_091812_catalog_initial_data cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200218_091812_catalog_initial_data cannot be reverted.\n";

        return false;
    }
    */
}
